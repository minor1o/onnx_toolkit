#include "parser.h"

int main(int argc, char *argv[]) {
  configuration config{NULL, NULL, NULL,
                       false, false, false,
                       false, false, false, false};
  parsing(argc, argv, config);
  // parse cmd args to configuration

  if (config.help) {
    return 0;
  }

  if (!config.model_path) {
    printf("Provide onnx_toolkit with *.onnx file path\n");
    return 0;
  }

  if (config.getup || config.bandwidth || config.chain || config.density) {
    onnx::ModelProto model;
    get_model(config.model_path, model);
    auto graph = model.graph();

    if (config.getup) {
      std::map<std::string, int> opdata = extract_opname(graph);
      // map of {operator_type, use_counter>}
      output_opdata(config, opdata);
    } else if (config.chain) { // config.chain = True
      std::map<std::string, std::vector<int>> opchain = get_chains(graph);
      output_chains(config, opchain);
    } else if (config.density) {
      std::map<std::string, int> dop = get_density_operators(config.density_operators_path);
      if (dop.empty()) {
        std::cout << config.density_operators_path << "is empty on operators or no file can be opened";
        return 0;
      }
      std::vector<char> density_marked = mark_density_operators(dop, graph);
      output_density(config, dop, density_marked);
    } else {
      return 0;
    }
  }

//  if(config.weights) {
//  } //TBA
  return 0;
}

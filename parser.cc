#include "parser.h"

void parsing(int argc, char *argv[], configuration &config) {
  char identifier;
  const char *model_path, *output_path, *density_operators_path;
  cag_option_context context;

  cag_option_prepare(&context, options, CAG_ARRAY_SIZE(options), argc, argv);

  while (cag_option_fetch(&context)) {
    identifier = cag_option_get(&context);
    switch (identifier) {
      case 'm':model_path = cag_option_get_value(&context);
        config.model_path = model_path;
        break;
      case 'd': density_operators_path = cag_option_get_value(&context);
        config.density_operators_path = density_operators_path;
        config.density = true;
        break;
      case 'f':output_path = cag_option_get_value(&context);
        config.output_path = output_path;
        break;
      case 'g':config.getup = true;
        break;
      case 'b':config.bandwidth = true;
        break;
      case 'w':config.weights = true;
        break;
      case 'u':config.sort_by_usage = true;
        break;
      case 'c': config.chain = true;
        break;
      case 'h':printf("Usage: main [OPTION]...\n");
        printf("Demonstrates the onnx_toolkit.\n\n");
        cag_option_print(options, CAG_ARRAY_SIZE(options), stdout);
        config.help = true;
        break;
    }
  }
}

void get_model(const std::string &model_path, onnx::ModelProto &model) {
  std::ifstream input(model_path, std::ios::ate | std::ios::binary);

  if (input.is_open()) {
    std::streamsize size = input.tellg(); // get current position in file
    input.seekg(0, std::ios::beg);        // move to start of file

    std::vector<char> buffer(size);
    input.read(buffer.data(), size);           // read raw data
    model.ParseFromArray(buffer.data(), size); // parse protobuf

    input.close();
  }
}

std::map<std::string, int> get_density_operators(const std::string &density_operators_path) {
  std::map<std::string, int> res;
  std::ifstream input(density_operators_path);

  if (input.is_open()) {
    std::string s;
    while (std::getline(input, s))
      if (res.find(s) == res.end())
        res.insert({s, 0});
  }
  return res;
}

std::vector<char> mark_density_operators(std::map<std::string, int> &dop, const onnx::GraphProto &graph) {
  std::vector<char> res(graph.node_size(), '0');
  for (int i = 0; i < graph.node_size(); ++i) {
    if (dop.find(graph.node(i).op_type()) != dop.end()) {
      res[i] = '1';
      dop[graph.node(i).op_type()]++;
    }
  }
  return res;
}

template<typename Arg, typename... Args>
void write(std::ostream &out, Arg &&arg, Args &&...args) {
  out << std::forward<Arg>(arg);
  ((out << std::forward<Args>(args)), ...);
}

std::map<std::string, int> extract_opname(const onnx::GraphProto &graph) {

  std::map<std::string, int> data{};
  for (int i = 0; i < graph.node_size(); ++i) {
    if (data.find(graph.node(i).op_type()) != data.end()) {
      ++data[graph.node(i).op_type()];
    } else {
      data.insert({graph.node(i).op_type(), 1});
    }
  }
  return data;
}

void dfs(int v, std::vector<std::vector<int>> &chain, std::vector<char> &vis,
         std::map<std::string, int> &gbo, const onnx::GraphProto &graph) {
  vis[v] = true;
  for (const auto &s : graph.node(v).output()) {
    if (!vis[gbo[s]]) dfs(gbo[s], chain, vis, gbo, graph);

    if (graph.node(v).op_type() != graph.node(gbo[s]).op_type())
      continue;
    if (chain[gbo[s]].empty())
      chain[v].push_back(1);
    else {
      for (const auto &so : chain[gbo[s]])
        chain[v].push_back(so + 1);
    }
  }
}

std::map<std::string, std::vector<int>> get_chains(const onnx::GraphProto &graph) {
  std::map<std::string, int> ind{};
  std::map<std::string, int> gbo{}, gbi{};
  for (int i = 0; i < graph.node_size(); ++i) {
    ind.insert({graph.node(i).name(), i});
    for (const auto &ins : graph.node(i).input())
      gbo[ins] = i;
    for (const auto &out : graph.node(i).output())
      gbi[out] = i;
  }
  std::vector<std::vector<int>> chain(graph.node_size());
  std::vector<char> vis(graph.node_size(), 0);

  for (int i = 0; i < graph.node_size(); ++i) {
    if (!vis[i]) {
      dfs(i, chain, vis, gbo, graph);
    }
  }
  std::map<std::string, std::vector<int>> data{};
  for (int i = 0; i < graph.node_size(); ++i) {
    char has_same = false;
    for (const auto &inp : graph.node(i).input()) {
      if (graph.node(i).op_type() == graph.node(gbi[inp]).op_type()) {
        has_same = true;
        break;
      }
    }
    if (has_same) continue;
    for (const auto c : chain[i]) {
      data[graph.node(i).op_type()].push_back(c);
    }
  }
  return data;
}

void output_opsz(const configuration &config,
                 const std::map<std::string, std::pair<long, long>> &mp) {
  std::ofstream output(config.output_path, std::ios::out);

  write(config.output_path ? output : std::cout,
        "OP,MaxInputSize,MaxOutputSize\n");

  for (auto g : mp) {
    write(config.output_path ? output : std::cout,
          g.first, ',', g.second.first, ',', g.second.second, '\n');
  }
}

void output_opdata(const configuration &config,
                   const std::map<std::string, int> &mp) {
  std::ofstream output(config.output_path, std::ios::out);

  write(config.output_path ? output : std::cout, "OP", ',', "CNT",
        "\n");
  for (auto g : mp)
    write(config.output_path ? output : std::cout, g.first, ',', g.second, '\n');
}

void output_chains(const configuration &config, std::map<std::string, std::vector<int>> &opchain) {
  std::ofstream output(config.output_path, std::ios::out);
  for (const auto &it : opchain) {
    write(config.output_path ? output : std::cout, it.first, ",");
    for (int i = 0; i < it.second.size(); ++i)
      write(config.output_path ? output : std::cout, it.second[i],
            (i == it.second.size() - 1) ? "" : ",");
    write(config.output_path ? output : std::cout, "\n");
  }
}

void output_density(const configuration &config,
                    const std::map<std::string, int> dop,
                    const std::vector<char> density) {
  std::ofstream output(config.output_path, std::ios::out);
  write(config.output_path ? output : std::cout, "Density:\n");
  for (const auto &c : density) {
    write(config.output_path ? output : std::cout, c);
  }
  write(config.output_path ? output : std::cout, "\nStatistics:\n");
  for (const auto &p : dop) {
    write(config.output_path ? output : std::cout, p.first, ":", p.second,
          "\n");
  }
}

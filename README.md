# onnx_toolkit

[//]: # (На вход программы подаются аргументы, например: `--omp <>`, `--output_path <>`, `--getup &#40;get unique operator&#41;` и тд. )

[//]: # ()

[//]: # (Каждый аргумент отвечает за конкретную задачу. Сначала остановимся на получении уникальных op из нейросети.  )


Supported args: \
Set onnx model path  `-m, -M, --omp=$PATH$` \
Set output path `-f, -F, -o, -O, --ofile=$PATH$` \
Get unique operator and it's usage count  `-g, -G, --getup`\
Get input and output max size of each operator `-b, -B, --bandwidth`

Get representation of density of listed operators in vector of consecutive topsorted nodes, 
value = {0, 1} if is not in list or in list respectively 
`-d, -D, --density=$PATH$           `

Get all lengths chains of consecutive operators `-c, -C, --chain`

> -1 means, that input/output tensor does not exist for such operator

Shows the command help `-h, --help`

TBA:\
Sort list of operations in model by usage `-u, -U, --usage`\
Get weights of model `-w, -W, --weights`\
[default is alphabetic sort of operations]

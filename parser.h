#ifndef MAIN__ARGS_H_
#define MAIN__ARGS_H_

#include "onnx.proto3.pb.h"
#include <cargs.h>
#include <fstream>
#include <iostream>
#include <map>
#include <set>

static struct cag_option options[] = {
    {.identifier = 'm',
        .access_letters = "mM",
        .access_name = "omp",
        .value_name = "$PATH$",
        .description = "Set onnx model path"},

    {.identifier = 'f',
        .access_letters = "fFoO",
        .access_name = "ofile",
        .value_name = "$PATH$",
        .description = "Set output path"},

    {.identifier = 'g',
        .access_letters = "gG",
        .access_name = "getup",
        .value_name = NULL,
        .description = "Get unique operator"},

    {.identifier = 'b',
        .access_letters = "bB",
        .access_name = "bandwidth",
        .value_name = NULL,
        .description = "Get bandwidth"},

    {.identifier = 'w',
        .access_letters = "wW",
        .access_name = "weights",
        .value_name = NULL,
        .description = "Get weights of model"},

    {.identifier = 'u',
        .access_letters = "uU",
        .access_name = "usage",
        .value_name = NULL,
        .description =
        "Sort list of operations in model by usage [default is alphabetic]"},

    {.identifier = 'c',
        .access_letters = "cC",
        .access_name = "chain",
        .value_name = NULL,
        .description = "Get longest chains of consecutive operators"},

    {.identifier = 'd',
        .access_letters = "dD",
        .access_name = "density",
        .value_name = "$PATH$",
        .description = "Get representation of density of listed operators in vector "
                       "of consecutive topsorted nodes, value = {0, 1} if is not in list or in list respectively"},

    {.identifier = 'h',
        .access_letters = "h",
        .access_name = "help",
        .description = "Shows the command help"}};

struct configuration {
  const char *model_path, *output_path, *density_operators_path;
  bool getup,  // get unique operator command
  bandwidth,  //get bandwidth
  weights, // get weights of model
  help,
  sort_by_usage,
  chain,
  density;
};

void parsing(int argc, char *argv[], configuration &config);

void get_model(const std::string &model_path, onnx::ModelProto &model);

std::map<std::string, int> get_density_operators(const std::string &density_operators_path);

std::vector<char> mark_density_operators(std::map<std::string, int> &dop, const onnx::GraphProto &graph);

std::map<std::string, int> extract_opname(const onnx::GraphProto &graph);

std::map<std::string, std::vector<int>> get_chains(const onnx::GraphProto &graph);

void output_opsz(const configuration &config,
                 const std::map<std::string, std::pair<long, long>> &mp);

void output_opdata(const configuration &config,
                   const std::map<std::string, int> &mp);

void output_chains(const configuration &config, std::map<std::string, std::vector<int>> &opchain);

void output_density(const configuration &config, const std::map<std::string, int> dop, const std::vector<char> density);

#endif // MAIN__ARGS_H_
